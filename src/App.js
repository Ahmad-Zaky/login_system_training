import React, { Component } from "react";
import Routes from "./components/Routes/routes";
// import { postLogin } from "./api/postLogin.js";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes />
      </div>
    );
  }
}

export default App;

/* -------- Deprecated Code -------- */

// Line: 17

// // handle the submit input for Login event
//   handleLogin = () => {
//     // Basic validation: check if username and password have values
//     if (this.state.username && this.state.password) {
//       // sending the type of data and the data it self
//       postLogin(this.state).then(result => {
//         let responseJSON = result;

//         // Basic validation: check if we did fetch some user data or not
//         if (responseJSON.userDate) {
//           sessionStorage.setItem("userDate", responseJSON);
//           this.setState({ redirect: true });
//         } else {
//           console.log("Login error!");
//         }
//       });
//     } else {
//       console.log("No username or password!");
//     }
//   };
