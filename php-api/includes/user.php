<?php 

    class User extends DB_object{
        
    // ------- Properties -------
        
        /* --- GENERIC NAMES --- */
        protected static $db_table_fields = array("id", "username", "password", "password_hash", "first_name", "second_name", "email", "registered_at", "updated_at");
        protected static $db_table = "users";


        /* --- Class Table Properties --- */
        
        // protected to let parent class handle child class properties 
        protected $id = 0;
        protected $username = "";
        protected $password = "";
        protected $password_hash = ""; /* Generic Name */
        protected $first_name = "";
        protected $second_name = "";
        protected $email = "";
        protected $registered_at = "";
        protected $updated_at = "";
        
        
        
        
        
    // ------- Methods -------
        
        // Magic method (constructor)
        function __construct(){
            
            $this->placeholder_photo = "usr.jpg"; 
            $this->upload_dir = "images"; 
            $this->placeholder_photo_url = "http://placehold.it/400X400?text=image"; 
        }
        
        // Magic methods (set and get)
        public function __set($property, $value){
            
            if(property_exists($this, $property))
                $this->$property = $value;
            else
                $this->errors[] = "Failed to set $property to $value!";
        }
        
        public function __get($property){
            
            if(property_exists($this, $property))
                return $this->$property;
            else
                $this->errors[] =  "Failed to get $property!";
        }
        
        // ---- Login Form function ---
        public static function login($usr, $pwd){


                $username = trim($usr); // using trim to remove and prefix white spaces 
                $password = trim($pwd);

                // check the user data with DB
                $user = self::verify_user($username, $password);

                // user found
                if($user){
                    return $user;

                }else{ 
                    // if not found then we have two possibilities 1. fields are empty. 2. wrong usr or pwd 
                    if(!empty($username) && !empty($password))
                        return 0;
                    else
                        return -1;
                }
            
        }
        
        // verify usr and pwd with DB
        public static function verify_user($username, $password){
            global $db;
            
            $username = $db->escape_string($username);
            $password = $db->escape_string($password);
            
            $query = "SELECT * FROM ". self::$db_table ." WHERE ";
            $query .= "username = '$username'";
            
            $user_found = User::makeQuery($query);
            
            $user = array_shift($user_found);
            
            if($user){
                
                // check if there is a password_hash
                if($user->password_hash !== ""){
                    
                    if(!password_verify($password, $user->password_hash))
                        return false;
                }else{
                    
                    if($password !== $user->password)
                        return false;
                    
                    // set a password hash if there is no one
                    $user->password_hash = $user->set_password_hash($password);
                    $user->save();
                }
                return $user;
            }
            return false;
        }
        
          
        // get all admin users from DB
        public static function get_admin_users(){
            
            global $db;
            
            $query = "SELECT * FROM users ";
            $query .= "WHERE user_role = 'admin'";
           
            $result = User::makeQuery($query);
            
            return $result;
        }
            
                /* ----- ABSTRACT METHODS ----*/

        
        // get Nr. of admin users
        public static function counter_approved(){
            
            global $db;
            
            $query = "SELECT COUNT(*) FROM users ";
            $query .= "WHERE (user_role = 'admin') ";
           
            $result = $db->query($query);
            
            $row = $result->fetch_array(MYSQLI_NUM);
            
            return !empty($row) ? $row[0] : false;
            
        }
        
                   
        // get Nr. of subscriber users
        public static function counter_unapproved(){
            
            global $db;
            
            $query = "SELECT COUNT(*) FROM users ";
            $query .= "WHERE (user_role = 'subscriber') ";
           
            $result = $db->query($query);
            
            $row = $result->fetch_array(MYSQLI_NUM);
            
            return !empty($row) ? $row[0] : false;
            
        }
         
                /* ----- /.ABSTRACT METHODS ----*/

        
    
    }

?>

