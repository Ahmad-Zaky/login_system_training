export function postLogin( userData ) {
    const baseURL = 'http://localhost:80/login-system-php-api/index.php';

    return new Promise((resolve, reject) => {
        fetch(baseURL, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify(userData)
        })
        .then((response) => response.json())
        .then((json) => {
            resolve(json);
        })
        .catch((error) => {
            reject(error);
        });
    });
}