<?php 
    
    require_once("includes/init.php");
    
    if(isset($_POST['username']) && isset($_POST['password'])){
        
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        $user = User::login($username, $password);
        
        if($user){
            $respnose = array("success" => true, "id" => $user->id, "username" => $user->username); 
            echo json_encode($respnose);
        }else{
            $respnose = array("success" => false, "message" => 'User not found!'); 
            echo json_encode($respnose);
        }
        
    }
    else{
        $respnose = array("success" => false, "message" => 'No Login data is recieved!'); 
        echo json_encode($respnose);
    }

?>



