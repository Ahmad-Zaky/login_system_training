<?php 
    
    require_once("includes/init.php");

    $users = User::find_all();


    $usersJSON = array();

    if($users){
        // turn user object into array
        foreach($users as $user)
        {
            $usersJSON[] = array(
                "id" => $user->id, 
                "username" => $user->username,
                "first_name" => $user->first_name,
                "second_name" => $user->second_name,
                "email" => $user->email,
                "registered_at" => $user->registered_at,
                "updated_at" => $user->updated_at
            );
        }
        
        $respnose = array("success" => true, "users" => $usersJSON); 
        echo json_encode($respnose);
    }else{
        $respnose = array("success" => false, "message" => 'Users not found!'); 
        echo json_encode($respnose);
    }
?>
