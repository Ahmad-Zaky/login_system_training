import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import {
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Input,
  Label
} from "reactstrap";

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      newUser: {
        username: "",
        password: "",
        confirm_password: "",
        first_name: "",
        second_name: "",
        email: "",
        registered_at: new Date()
          .toISOString()
          .slice(0, 19)
          .replace("T", " "),
        updated_at: "0000-00-00 00:00:00"
      },
      editUser: {
        id: 0,
        username: "",
        password: "",
        confirm_password: "",
        first_name: "",
        second_name: "",
        email: "",
        registered_at: "0000-00-00 00:00:00",
        updated_at: new Date()
          .toISOString()
          .slice(0, 19)
          .replace("T", " ")
      },
      LoggedInUsername: "",
      newUserModal: false,
      editUserModal: false,
      redirectToReferrer: false
    };
  }

  // get User data if we recieved data from sessionStorage()
  componentWillMount() {
    if (sessionStorage.getItem("userData")) {
      this.getUserFeed();
    } else {
      this.setState({ redirectToReferrer: true });
    }
  }

  // when redirect we clear our session and redirect back to login
  logout = () => {
    sessionStorage.setItem("userData", "");
    sessionStorage.clear();
    this.setState({ redirectToReferrer: true });
  };

  // To close the add Modal after opening it
  toggleNewUserModal = () => {
    this.setState({ newUserModal: !this.state.newUserModal });
  };

  // To close the edit Modal after opening it
  toggleEditUserModal = () => {
    this.setState({ editUserModal: !this.state.editUserModal });
  };

  // To open the edit Modal
  EditUserModal = user => {
    const {
      id,
      username,
      first_name,
      second_name,
      email,
      registered_at,
      updated_at
    } = user;

    this.setState({
      editUser: {
        id,
        username,
        password: "",
        confirm_password: "",
        first_name,
        second_name,
        email,
        registered_at,
        updated_at
      },
      editUserModal: !this.state.editUserModal
    });
  };

  /* -------- CRUD HANDLE FUNCTIONS -------- */

  // ------
  // CREATE
  // ------

  // store input from add form into state props
  handleInputChange = e => {
    let { newUser } = this.state;
    newUser[e.target.name] = e.target.value;
    this.setState({ newUser });
  };

  // TODO: do I need to reset the newUser to empty string
  handleAddUser = e => {
    e.preventDefault();

    // delete the confirm_password attribute from the newUser object
    let newUser = this.state.newUser;
    delete newUser.confirm_password;

    const baseURL =
      "http://localhost:80/login-system-training/php-api/add_user.php";

    axios
      .post(baseURL, this.state.newUser)
      .then(response => {
        if (response.data.success) {
          // render the new created user
          const { users } = this.state;
          users.push(response.data.newUser);
          this.setState({ users, newUserModal: false });
        } else {
          console.log("Failed to create a new user!", response.data);
        }
      })
      .catch(error => console.log(error));
  };

  // ----
  // READ
  // ----

  // extract the logged in username from sessionStorage() and fetch all users from php-api
  getUserFeed = () => {
    // get logged in user
    let user = JSON.parse(sessionStorage.getItem("userData"));
    if (user.data) {
      // get logged in username
      this.setState({ LoggedInUsername: user.data.username });

      // get users from php-api
      const baseURL =
        "http://localhost:80/login-system-training/php-api/users.php";
      axios.get(baseURL).then(response => {
        if (response.data.success) {
          this.setState({
            users: response.data.users
          });
        } else {
          console.log("No users recieved from php-api");
        }
      });
    } else {
      console.log("No Data recieved from Login!");
    }
  };

  // ------
  // UPDATE
  // ------

  // store input from edit form into state props
  handleEditInputChange = e => {
    let { editUser } = this.state;
    editUser[e.target.name] = e.target.value;
    this.setState({ editUser });
  };


  // TODO: Add new_password <input> and state.edituser attribute
  handleEditUser = () => {
    // delete the confirm_password attribute from the newUser object
    let editUser = this.state.editUser;
    delete editUser.confirm_password;

    const baseURL =
      "http://localhost:80/login-system-training/php-api/edit_user.php";
    axios
      .put(baseURL, this.state.editUser)
      .then(response => {
        if (response.data.success) {
          // get the id and user from php-api
          let id = response.data.editUser["id"];
          let user = response.data.editUser;

          // get users from frontend and set the updated data using id
          const users = Object.assign([], this.state.users)
          const index = users.findIndex(user => user.id == id);
          users[index] = user;

          // turn off the modal
          this.setState({ users, editUserModal: false });
        } else {
          console.log("Failed to upadate the user!", response.data);
        }
      })
      .catch(error => console.log(error));
  };

  // ------
  // DELETE
  // ------

  handleDeleteUser = id => {
    // remove the user from the frontend
    let users = Object.assign([], this.state.users);
    users = users.filter(u => u.id !== id);
    this.setState({ users });

    // remove the user from the backend

    const baseURL =
      "http://localhost:80/login-system-training/php-api/delete_user.php";
    axios.delete(baseURL, { data: { id: id } }).then(response => {
      if (response.data) {
        console.log(response.data);
      } else {
        console.log("Failed to delete the user!", response.data);
      }
    });
  };

  /* -------- ./CRUD HANDLE FUNCTIONS -------- */

  render() {
    // Redirect to Login if session key is empty or in case of logout
    if (this.state.redirectToReferrer) {
      return <Redirect to="/Login" />;
    }

    // render the users
    let users = this.state.users.map(user => {
      return (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.username}</td>
          <td>{user.first_name}</td>
          <td>{user.second_name}</td>
          <td>{user.email}</td>
          <td>{user.registered_at}</td>
          <td>{user.updated_at}</td>
          <td>
            <Button
              color="success"
              size="sm"
              className="m-2"
              onClick={() => {
                this.EditUserModal(user);
              }}
            >
              Edit
            </Button>
            <Button
              color="danger"
              size="sm"
              onClick={() => {
                this.handleDeleteUser(user.id);
              }}
            >
              Delete
            </Button>
          </td>
        </tr>
      );
    });

    // for edit modal to render the input values
    const { username, first_name, second_name, email } = this.state.editUser;
    return (
      <React.Fragment>
        <h1>Welcome {this.state.LoggedInUsername}</h1>
        <button type="button" className="btn btn-primary" onClick={this.logout}>
          Logout
        </button>

        <hr />

        <Button
          color="primary"
          onClick={this.toggleNewUserModal}
          className="m-2"
        >
          Add User
        </Button>
        {/*  --- ADD ENW USER  --- */}
        <Modal
          isOpen={this.state.newUserModal}
          toggle={this.toggleNewUserModal}
        >
          <ModalHeader toggle={this.toggleNewUserModal}>Add User</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="first_name">First Name</Label>
              <Input
                type="text"
                name="first_name"
                id="first_name"
                placeholder="Please enter your first name"
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="second_name">Second Name</Label>
              <Input
                type="text"
                name="second_name"
                id="second_name"
                placeholder="Please enter your second name"
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                placeholder="Please enter your email"
                onChange={this.handleInputChange}
              />
            </FormGroup>

            <hr />

            <FormGroup>
              <Label for="username">Username</Label>
              <Input
                type="text"
                name="username"
                id="username"
                placeholder="Please enter your username"
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="text"
                name="password"
                id="password"
                placeholder="Please enter your password"
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="confirm_password">Confirm Password</Label>
              <Input
                type="text"
                name="confirm_password"
                id="confirm_password"
                placeholder="Please confirm your password"
                onChange={this.handleInputChange}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleAddUser}>
              Add User
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleNewUserModal}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/*  ./--- ADD ENW USER  --- */}

        {/*  --- EDIT USER  --- */}

        <Modal
          isOpen={this.state.editUserModal}
          toggle={this.toggleEditUserModal}
        >
          <ModalHeader toggle={this.toggleEditUserModal}>Edit User</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="first_name">First Name</Label>
              <Input
                type="text"
                name="first_name"
                id="first_name"
                value={first_name}
                onChange={this.handleEditInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="second_name">Second Name</Label>
              <Input
                type="text"
                name="second_name"
                id="second_name"
                value={second_name}
                onChange={this.handleEditInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                value={email}
                onChange={this.handleEditInputChange}
              />
            </FormGroup>

            <hr />

            <FormGroup>
              <Label for="username">Username</Label>
              <Input
                type="text"
                name="username"
                id="username"
                value={username}
                onChange={this.handleEditInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="text"
                name="password"
                id="password"
                placeholder="Please enter your new password"
                onChange={this.handleEditInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="confirm_password">Confirm Password</Label>
              <Input
                type="text"
                name="confirm_password"
                id="confirm_password"
                placeholder="Please confirm your password"
                onChange={this.handleEditInputChange}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.handleEditUser()}>
              Edit User
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleEditUserModal}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/*  ./--- EDIT USER  --- */}

        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>First name</th>
              <th>Second name</th>
              <th>Email</th>
              <th>Registered at</th>
              <th>Updated at</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>{users}</tbody>
        </Table>
      </React.Fragment>
    );
  }
}

export default Users;
