<?php 

    require_once("includes/init.php");

    $newUser = json_decode(file_get_contents("php://input"), true);

    if(isset($newUser) && !empty($newUser)){
        
        $newUser = User::create_obj($newUser);
        if($newUser)
        {
            // set password hash
            $newUser->password_hash = $newUser->set_password_hash($newUser->password);
            
            $newUser->save();
            
            // returning the user information to be rendered to the user
            $newUserJSON = array(
                "id" => $newUser->id, 
                "username" => $newUser->username,
                "first_name" => $newUser->first_name,
                "second_name" => $newUser->second_name,
                "email" => $newUser->email,
                "registered_at" => $newUser->registered_at,
                "updated_at" => $newUser->updated_at
            );
            
            $response = array("success" => true, "message" => "adding a new user was successful", "newUser" => $newUserJSON);
            echo json_encode($response);     
        }else{
            $respnose = array("success" => false, "message" => "Failed to save a new created user!"); 
            echo json_encode($respnose);
        }
    }else{
        $respnose = array("success" => false, "message" => "Failed to create a new user!"); 
        echo json_encode($respnose);
    }

?>




