import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import Login from "../Login/login";
import Users from "../Users/users";

const Routes = () => (
  <Router>
    <Switch>
      <Route path="/Login" component={Login} />
      <Route path="/Users" component={Users} />
      <Link to="/Login">Login</Link>
    </Switch>
  </Router>
);

export default Routes;
