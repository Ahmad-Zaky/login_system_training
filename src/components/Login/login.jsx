import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      redirect: false
    };
  }

  // send input data to PHP API and reseve a response with the user data
  handleLogin = e => {
    // check if login data are empty
    if (this.state.username && this.state.password) {
      e.preventDefault();

      let formData = new FormData();
      formData.append("username", this.state.username);
      formData.append("password", this.state.password);
      const baseURL =
        "http://localhost:80/login-system-training/php-api/login.php";

      axios
        .post(baseURL, formData)
        .then(result => {
          if (result.data.success) {
            console.log(result.data);
            sessionStorage.setItem("userData", JSON.stringify(result));
            this.setState({ redirect: true });
          } else {
            console.log("Login error! ", result.data.success);
          }
        })
        .catch(error => console.log(error));
    } else console.log("state data is empty!");
  };

  // get the name of input tag  and its value and setState it
  handleInputChange = async e =>
    await this.setState({ [e.target.name]: e.target.value });

  render() {
    // image path var
    var divStyleImg = "login/images/bg-01.jpg";

    // redirect to users after success login
    if (this.state.redirect) {
      return <Redirect to={"./Users"} />;
    }

    // redirect to users if still login
    if (sessionStorage.getItem("userData")) {
      return <Redirect to={"./Users"} />;
    }

    return (
      <div className="limiter">
        <div className="container-login100">
          <div className="wrap-login100">
            <div
              className="login100-form-title"
              style={{
                backgroundImage: 'url("' + divStyleImg + '")'
              }}
            >
              <span className="login100-form-title-1">Sign In</span>
            </div>

            <form className="login100-form validate-form">
              <div
                className="wrap-input100 validate-input m-b-26"
                data-validate="Username is required"
              >
                <span className="label-input100">Username</span>
                <input
                  className="input100"
                  type="text"
                  name="username"
                  placeholder="Enter username"
                  onChange={this.handleInputChange}
                />
                <span className="focus-input100"></span>
              </div>

              <div
                className="wrap-input100 validate-input m-b-18"
                data-validate="Password is required"
              >
                <span className="label-input100">Password</span>
                <input
                  className="input100"
                  type="password"
                  name="password"
                  placeholder="Enter password"
                  onChange={this.handleInputChange}
                />
                <span className="focus-input100"></span>
              </div>

              <div className="flex-sb-m w-full p-b-30">
                <div className="contact100-form-checkbox">
                  <input
                    className="input-checkbox100"
                    id="ckb1"
                    type="checkbox"
                    name="remember-me"
                  />
                  <label className="label-checkbox100" htmlFor="ckb1">
                    Remember me
                  </label>
                </div>

                <div>
                  <a href="#" className="txt1">
                    Forgot Password?
                  </a>
                </div>
              </div>

              <div className="container-login100-form-btn">
                <button
                  className="login100-form-btn"
                  onClick={this.handleLogin}
                >
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;

//  - TODO List:

//      1. enable javascript validation in React - javascript files downloaded with Login Form Template -
//      2. Solve the redirecting 'localhost:3000/Login/' problem to prevent going to another login source.
