<?php 

    require_once("includes/init.php");

    $editUser = json_decode(file_get_contents("php://input"), true);
    
    if(isset($editUser) && !empty($editUser)){
        
        $user = User::find_byID($editUser['id']);
        if($user)
        {
            
            // set the new user values
            $user->username = $editUser['username'];
            $user->first_name = $editUser['first_name'];
            $user->second_name = $editUser['second_name'];
            $user->email = $editUser['email'];
            $user->updated_at = $editUser['updated_at'];
            
            // set password hash
            if($editUser['password']){
                $user->password = $editUser['password'];   
                $user->password_hash = $user->set_password_hash($editUser['password']);
            }
            
            $user->save();
            
            // returning the user information to be rendered to the user
            $editUserJSON = array(
                "id" => $user->id, 
                "username" => $user->username,
                "first_name" => $user->first_name,
                "second_name" => $user->second_name,
                "email" => $user->email,
                "registered_at" => $user->registered_at,
                "updated_at" => $user->updated_at
            );
            
            $response = array("success" => true, "message" => "updating a user was successful", "editUser" => $editUserJSON);
            echo json_encode($response);     
        }else{
            $respnose = array("success" => false, "message" => "Failed to save updated user!"); 
            echo json_encode($respnose);
        }
    }else{
        $respnose = array("success" => false, "message" => "Failed to update user!"); 
        echo json_encode($respnose);
    }

?>




