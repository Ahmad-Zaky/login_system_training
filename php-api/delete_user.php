<?php 

    require_once("includes/init.php");

    $deleteUserID = json_decode(file_get_contents("php://input"), true);
//    echo json_encode(array("success" => $deleteUserID));

    if($deleteUserID){
        
        $id = intval($deleteUserID['id']);
        $user = User::find_byID($id);
        
        if($user)
        {
            // delete the user from the database
            $user->delete();
            
            $response = array("success" => true, "message" => "deleting the user was successful");
            echo json_encode($response);     
        }else{
            $respnose = array("success" => false, "message" => "Failed to delete the user!"); 
            echo json_encode($respnose);
        }
    }else{
        $respnose = array("success" => false, "message" => "Failed to find the user in order to delete it!"); 
        echo json_encode($respnose);
    }

?>




